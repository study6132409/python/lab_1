some_dict = {
    'first': 'some_value',
    'name': 'Anton',
    'age': 36,
    'country': 'Russia',
    'gender': 'M',
    'city': 'Moscow'
}

some_list = ['city', 'gender']


def get_keys():
    for key in some_dict.keys():
        print(key)


def get_keys_2():
    count = 0
    for key in some_dict.keys():
        if count % 2 == 0:
            print(key)

        count += 1


def is_key_in_dict():
    key = 'age'
    if key in some_dict.keys():
        print('Exists')


def is_key_and_value_in_dict():
    key = 'age'
    value = 36
    if key in some_dict.keys():
        if some_dict[key] == value:
            print('Exists')


def check_list_in_dict():
    for key in some_list:
        if key in some_dict.keys():
            print(some_dict[key])


check_list_in_dict()
