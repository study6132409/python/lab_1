# helpers
some_string = 'adsadad21adas9a8d'
random_list = [21, 98, 0.972, 'some', 'ss', False, str(), list()]
numbers = []
for n in range(0, 10):
    numbers.append(str(n))


def _is_number(value):
    if type(value) == int:
        return True
    for char in str(value):
        if char not in numbers:
            return False
    return True


def _get_number_list() -> []:
    is_enter_value = True
    value_list = []
    while is_enter_value:
        value = input('Введите число, для прекращения ввода введите "q": ')
        if '-' in value and value[0] == '-' and _is_number(value[1:]):
            value_list.append(int(value))
            continue
        if value == 'q' or value == 'Q':
            return value_list
        if not _is_number(value):
            print(f'Символ {value} не число. Игнорирую.')
            continue
        value_list.append(int(value))


def get_even():
    int_array = range(0, 100)
    even_array = []

    for number in int_array:
        if number % 2 == 0:
            even_array.append(number)

    print('Result: ', even_array)


def get_number():
    for char in some_string:
        if str(char) in numbers:
            print(char)


def three_times():
    for char in some_string:
        print(char * 3)


def is_empty():
    text = str(input('Введите текст: '))
    if text == '':
        print('True')
        return
    print('False')


def get_even_2():
    random_list = _get_number_list()
    for number in random_list:
        if number % 2 == 0:
            print(number)


def get_even_3():
    random_list = _get_number_list()
    for number in random_list:
        if number % 2 == 0:
            print(number)
        else:
            print(number + 1)


def get_positive():
    random_list = _get_number_list()
    for number in random_list:
        if number > 0:
            print(number)


def get_only_numbers():
    for value in random_list:
        if type(value) == int:
            print(value)


def count_python():
    for_loop_len = 0
    while_len = 0
    text = 'Python - это Питон!'

    for _ in text:
        for_loop_len += 1

    while while_len < len(text):
        while_len += 1

    print(f'For_loop: {for_loop_len}')
    print(f'While_loop: {while_len}')


def count_tuple():
    tuple_count = 0
    list_of_values = [(1, 2), (3), (4,), (5+6), (7+8,)]

    for value in list_of_values:
        if isinstance(value, tuple):
            tuple_count += 1

    print(f'Tuple count: {tuple_count}')


def print_net():
    # /\_/\_/\
    # \/ \/ \/
    net_lines = 6
    cell_count = 6

    for net in range(0, net_lines):
        print('/\\_' * cell_count)
        print('\\/ ' * cell_count)


print_net()
