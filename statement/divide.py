def divide():
    divide_value = float(input('Введите что делимое: '))
    divider_value = float(input('Введите делитель: '))

    try:
        print(f'{divide_value} / {divider_value} = {divide_value / divider_value}')
    except ZeroDivisionError:
        print('Ошибка, делить на ноль нельзя!')
