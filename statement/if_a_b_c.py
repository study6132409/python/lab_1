def text_in(text: str, *args) -> bool:
    for search in args:
        if search in text:
            return True
    return False


def if_a_b_c():
    input_value = str(input('Введите текст: '))

    if text_in(input_value, 'А', 'Б', 'В'):
        print('"А" или "Б" или "В" присутствует в тексте')
