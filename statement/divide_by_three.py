def divide_by_three():
    divide_value = float(input('Введите что делимое: '))
    divider_value = 3

    try:
        print(f'Число {divide_value} {"" if divide_value % divider_value == 0 else "не "}кратно {divider_value}')
    except ZeroDivisionError:
        print('Ошибка, делить на ноль нельзя!')
