def get_only_numbers():
    value = str(input('Введите строку: '))
    for char in value:
        try:
            if type(int(char)) != int:
                print(char)
                break
        except:
            continue


def get_warning():
    warning = 'сообщение'
    text = 'У вас есть переменная, в которой хранится текст. И есть переменная в которой хранится набор запрещенных слов.  Задача пройти по ней циклом и если встретите запрещенное слово – выдать пользователю сообщение о проблеме. Текст и слово – придумайте сами или возьмите из интернета. '

    current = ''
    for char in text:
        if char == ' ':
            current = ''
        current = current + str(char)
        if warning in current:
            print('Есть запрещенное слово')
            break

    if warning in text:
        print('Есть запрещенное слово')

get_warning()
